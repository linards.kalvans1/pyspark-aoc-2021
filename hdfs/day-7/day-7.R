library(stringr)
library(magrittr)
crab.positions <- readLines("hdfs/day-7/input-day-7.txt") %>%
  str_split(",") %>%
  unlist() %>%
  as.numeric()


test.positions <- c(16,1,2,0,4,2,7,1,2,14)

## PART 1

minDistance <- function(pos) {
  ## f(x) = abs(x1 - x) + abs(x2 - x) + abs(x3 - x) + ...
  ## f'(x) = sgn(x1 - x) + sgn(x1 - x) + abs(x1 - x)
  derivative <- sapply(min(pos):max(pos), function(x){
    sum(pos > x) - sum(pos < x)
  }) %>%
    setNames(min(pos):max(pos))
  
  # print(derivative)
  min.pos <- as.numeric(names(derivative)[abs(derivative) == min(abs(derivative))])
  # print(min.pos)
  sum(abs(pos - min.pos[1]))
}


minDistance(test.positions)
minDistance(crab.positions)


## Part 2

## f(x) = abs(x1 - x)*(1 + abs(x1 - x)) / 2 ~ abs(x1 - x) + (x1 - x)^2
## f'(x) = sgn(x1 - x) + 2(x1 - x)

fuelPt2 <- function(pos, x) {
  sum(abs(pos - x) + (pos - x)**2) / 2
}

fuelPt2(test.positions, 5)

minFuelPt2 <- function(pos){
  derivative <- sapply(min(pos):max(pos), function(x){
    sum(pos > x) - sum(pos < x) + 2*sum(pos - x)
  }) %>% setNames(min(pos):max(pos))
  # print(derivative)
  
  min.pos <- as.numeric(names(derivative)[abs(derivative) == min(abs(derivative))])
  # print(abs(derivative))
  fuelPt2(pos, min.pos[1])
}

minFuelPt2(test.positions)
minFuelPt2(crab.positions)
